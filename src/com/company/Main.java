package com.company;

public class Main {

    private static void bubble(int size){
        //TASK 1
        //отптимизированный пузырек
        //заполнение массива
        System.out.println("\n\nПузырьковая:");
        int[] arr_1 = new int[size];
        for (int i = 0; i < size; i++) {
            arr_1[i] = 1 + (int) (Math.random() * 100);
        }

        //пузырьковая сортировка
        int swap = 0;
        int teamp;
        int count = size - 1;
        while (count > 0){
            for (int i = 0; i < count; i++) {
                if (arr_1[i] > arr_1[i + 1]) {
                    teamp = arr_1[i];
                    arr_1[i] = arr_1[i + 1];
                    arr_1[i + 1] = teamp;

                    swap++;  //+1 свап
                }
            }
            count--;
        }


        System.out.println("Количество свапов пузырьковой:" + swap);
        //end TASK 1
    }


    private static void shakerSorting(int size){
        //TASK 2
        System.out.println("\n\nШейкерная:");
        int[] arr_2 = new int[size];
        for (int i = 0; i < size; i++) {
            arr_2[i] = 1 + (int) (Math.random() * 100);
        }

        int teamp;
        int countForward = size - 1;
        int countBack = 0;
        int swap = 0;
        while (countForward > 0 && countForward != countBack){
            for (int i = 0; i < countForward; i++) {
                if (arr_2[i] > arr_2[i + 1]) {
                    teamp = arr_2[i];
                    arr_2[i] = arr_2[i + 1];
                    arr_2[i + 1] = teamp;

                    swap++;     //+1 перестановка
                }
            }
            countForward--;

            for (int i = countForward; 0 < i; i--) {    //+1 перестановка
                if (arr_2[i] > arr_2[i - 1]) {
                    teamp = arr_2[i];
                    arr_2[i] = arr_2[i - 1];
                    arr_2[i - 1] = teamp;

                    swap++;     //+1 перестановка
                }
            }
            countBack++;

        }

        System.out.println("Количество свапов шейкерная:" + swap);
        //end //TASK 2
    }



    private static void inserts(int size){
        //TASK 3
        System.out.println("\n\nВставками:");
        int[] arr_2 = new int[size];
        for (int i = 0; i < size; i++) {
            arr_2[i] = 1 + (int) (Math.random() * 100);
        }


        int teamp;
        int swap = 0;
        for(int i = 0; i < size; i++){
           for(int j = i; j > 0; j--){
               if (arr_2[j] < arr_2[j - 1]) {
                   teamp = arr_2[j];
                   arr_2[j] = arr_2[j - 1];
                   arr_2[j - 1] = teamp;

                   swap++; //+1 перестановка
               }
           }
        }


        System.out.println("Количество свапов вставками:" + swap);
        //end TASK 3
    }




    public static void main(String[] args) {

        //TASK 1 пузырек
        bubble(1000);
        bubble(10000);
        //bubble(1000000);
        //TASK 1 пузырек


        //TASK 2 шейкерная сортировка
        shakerSorting(1000);
        shakerSorting(10000);
        //shakerSorting(1000000);
        //end TASK 2 шейкерная сортировка


        //TASK 3 сортировка вставками
        inserts(1000);
        inserts(10000);
        //inserts(1000000);
        //TASK 3 сортировка вставками

    }


}
